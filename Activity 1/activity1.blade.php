<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Blade Code</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    </head>
    <body >
        <style>
            body {
                font-family: sans-serif;
                font-size: 25px;
            }
            form{
                width: 700px;
                height: 700px;
                border: 3px solid #ccc;
                padding: 30px;
                background: #fff;
                border-radius: 15px;
                margin-left: 300px;
}
            }
        </style>
    </head>
    <body>
        <form method="POST">
            <h3>Personal Information</h3>
            <div class="row justify-content-center">
                <div class="col-md-10 p-1"> 
                    <label> Firstname:</label><br>
                    <input type="text" class="form-control" name="fname" placeholder="Enter your Firstname" >
                </div>
                    <div class="col-md-10 p-1"> 
                    <label>Lastname: </label><br>
                    <input type="text" class="form-control"  name="lname" placeholder="Enter your Lastname" >
                </div>
                <div class="col-md-10 p-1"> 
                    <label>Username: </label><br>
                    <input type="text" class="form-control" name="uname" placeholder="Enter your username">
                </div>
                <div class="col-md-10 p-1"> 
                    <label>Password: </label><br>
                    <input type="Password"  class="form-control" name="password" placeholder="Enter your Password">
                </div>
                <br>
                <div class="col-md-10 p-1">
                <textarea name="comments"></textarea>
                </div>
                <div class="col-md-10 p-1"> 
                    <label>Birthday: </label><br>
                    <input type="date"  class="form-control" name="password" placeholder="dd/mm/yyyy">
                </div>
                <div class="col-md-10 p-1">
                    <label>Town</label><br>
                    <select name="CivilStatus" class="form-select">
                        <option value="">Select Town</option>
                        <option value="Manaoag">Manaoag</option>
                        <option value="San Jacinto">Binalonan</option>
                        <option value="Urdaneta">Urdaneta</option>
                        <option value="Calasio">Urdaneta</option>
                         <option value="San Carlos">Villasis</option>
                        </select>
                </div>
                <div class="col-md-10 p-1">
                <button type="submit" name="click">Click me</button>
                <button type="submit" name="reset">Reset</button>
                <button type="submit" name="pindot me">Pindot me</button>
                </div>

    </body>
</html>
