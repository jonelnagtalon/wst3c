<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Route::view('/', 'home'); 
Route::view('/login', 'login'); 
Route::view('/register', 'register'); 
Route::view('/about', 'about'); 
Route::view('/contact', 'contact'); 



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::Get('/',function()  
{  
    return view('student');  
});  
  Route::get('student/details',function()  
  {  
    $url=route('student.details');  
   return $url;  
  })->name('student.details');
  */
  
  Route::view('/item', 'item'); 
Route::view('/customer', 'customer'); 
Route::view('/order', 'order'); 
Route::view('/orderdetails', 'orderdetails');
Route::get('customer/{cusid?}/{cusname?}/{cusad?}', function($cusid = '1',$cusname = 'cus1',$cusad = 'binalonan')
{
    return view('customer');
});
Route::get('item/{itemid?}/{itemname?}/{price?}', function($itemid = '1',$itemname = 'item1',$price = '999')
{
    return view('item');
});
Route::get('order/{cusid?}/{cusname?}/{ordnum?}/{date?}', function($cusid = '1',$cusname = 'cus1',$ordnum = '1',$date = '01-01-2022')
{
    return view('order');
});
Route::get('orderdetails/{transno?}/{itemid?}/{ordnum?}/{name?}/{price?}', function($transno = '1',$itemid = '1',$ordnum = '1',$name = 'item1',$price = '999')
{
    return view('orderdetails');
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('item');