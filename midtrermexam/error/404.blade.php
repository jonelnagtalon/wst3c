<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Inter:wght@200;300;400,500;700&display=swap" rel="stylesheet">
    <title>404 nOT FOUND</title>
</head>
<body>
<div id="error-404">
    <div class="container error-404">
        <div class="row">
        <div class="circles">
      <p>404<br>
       <small>PAGE NOT FOUND</small>
      </p>
      <span class="circle big"></span>
      <span class="circle med"></span>
      <span class="circle small"></span>
    </div>
                    </div>
    </div>
</div>
</body>
</html>