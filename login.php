<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>sorry sir rush</title>
    <link rel="stylesheet" href="http://bootswatch.com/darkly/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v13.0&appId=496223335357543&autoLogAppEvents=1" nonce="ck6NwMu8"></script>
  </head>
  <body>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '496223335357543',
          cookie     : true,
          xfbml      : true,
          version    : 'v2.8'
        });

        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));

       function statusChangeCallback(response){
         if(response.status === 'connected'){
           console.log('Logged in and authenticated');
           setElements(true);
           testAPI();
         } else {
           console.log('Not authenticated');
           setElements(false);
         }
       }

      function checkLoginState() {
        FB.getLoginStatus(function(response) {
          statusChangeCallback(response);
        });
      }

      function testAPI(){
        FB.api('/me?fields=name,email', function(response){
          if(response && !response.error){
            //console.log(response);
            buildProfile(response);
          }

        })
      }

      function buildProfile(user){
          let profile = `
          <h3>${user.name}</h3>
          <ul class="list-group">
            <li class="list-group-item">User ID: ${user.id}</li>
            <li class="list-group-item">Email: ${user.email}</li>
          </ul>
        `;

        document.getElementById('profile').innerHTML = profile;
         
      }

      function setElements(isLoggedIn){
        if(isLoggedIn){
          document.getElementById('logout').style.display = 'block';
          document.getElementById('fb-btn').style.display = 'none';
        } else {
          document.getElementById('logout').style.display = 'none';
          document.getElementById('fb-btn').style.display = 'block';
        }
      }

      function logout(){
        FB.logout(function(response){
          setElements(false);
        });
      }
    </script>
<?php 
$name="Jonel G. Nagtalon";
$YearSec="BSIT-3C";
$time="10:00 PM";
$date="March 07, 2022";?>
<div class="row justify-content-center">
<div class="col-md-7 bg-white p-5">
  <br><br><br><br>
  <div class="container alert alert-success">
    <br><br>
       <div>
        <center>   
          <form>
          <h4>My Info</h4>
          <br><br>
          <label>Name</label>
          <input type="text" value="<?php echo $name;?>" size="50" readonly>
        <br><br>
        <label>Section</label>
          <input type="text" value="<?php echo $YearSec;?>" size="50" readonly>
         <br><br>
          <label>Time</label>
          <input type="text" value="<?php echo $time;?>" size="50" readonly>
         <br><br>
          <label>Date</label>
          <input type="text" value="<?php echo $date;?>" size="50" readonly>
             <br><br>
            <fb:login-button name="fb"
              id="fb-btn"
              scope="email"
              onlogin="checkLoginState();">
            </fb:login-button>
            <a id="logout" href="#" onclick="logout()">Logout</a>
            </form>  
          </div>
        </div>
    </div>
    </div>
</div>
  </body>
</html>
      

