<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Route::view('/', 'home'); 
Route::view('/login', 'login'); 
Route::view('/register', 'register'); 
Route::view('/about', 'about'); 
Route::view('/contact', 'contact'); 


/*
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::Get('/',function()  
{  
    return view('student');  
});  
  Route::get('student/details',function()  
  {  
    $url=route('student.details');  
   return $url;  
  })->name('student.details');
*/

Route::get('/',[OrderController::class, 'item' ]);
Route::get('/customer',[OrderController::class, 'customer' ]);
Route::get('/order',[OrderController::class, 'order' ]);
Route::get('/orderdetails',[OrderController::class, 'orderdetails' ]);
Auth::routes();

