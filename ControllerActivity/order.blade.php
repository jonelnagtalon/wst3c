<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('', 'Item') }}
                </a>
                <a class="navbar-brand" href="{{ url('/customer') }}">
                    {{ config('', 'Customer') }}
                </a>
                <a class="navbar-brand" href="{{ url('/order') }}">
                    {{ config('', 'Order') }}
                </a>
                <a class="navbar-brand" href="{{ url('/orderdetails') }}">
                    {{ config('', 'Orderd Details') }}
                </a>
              
            </div>
        </nav>
    </div>
</body>

<h1><b>Order</b></h1>

<h3><?php 
echo "Customer ID: ";
echo $cusid;
echo "<br>";
echo "Customer Name :  ";
echo $cusname;
echo "<br>";
echo "Order No. : ";
echo $ordnum;
echo "<br>";
echo "Date : ";
echo $date;

?></h3>
